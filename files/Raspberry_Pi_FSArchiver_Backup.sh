#!/bin/bash
#
BASEDIR=/mnt/nas/archive/raspberry_backups
DIR=${BASEDIR}/$(hostname -s)
OS_REL=$(lsb_release -si)
OS_VERSION=$(lsb_release -rs | cut -d . -f 1)

# checking if pv, parted and fsarchiver are installed
for APP in pv parted fsarchiver; do
   if ! which $APP &>/dev/null; then
      echo "$APP is not installed! Giving up"
      exit 1
   fi
done

MOUNTPOINT=/mnt/nas/archive
check_mount() {
   grep -q $MOUNTPOINT /proc/mounts
}

# https://stackoverflow.com/a/12199798/3151187
function convertsecs() {
   ((h = ${1} / 3600))
   ((m = (${1} % 3600) / 60))
   ((s = ${1} % 60))
   printf "%02d hours, %02d minutes and %02d seconds\n" $h $m $s
}

# mounting NAS
check_mount || mount $MOUNTPOINT
check_mount || echo "Backup failed! Cannot mount $MOUNTPOINT"
check_mount || exit 1

echo "Starting RaspberryPI backup process!"

# clean-up package dir
if [[ "$OS_REL" == 'Arch' ]]; then
   pacman -Scc --noconfirm
elif [[ "$OS_REL" == 'Ubuntu' ]]; then
   aptitude clean
fi

# mkdir -p won't fail if the directory exists
mkdir -p $DIR

# Create a filename with datestamp for our current backup (without .fsa suffix)
OUTFILE_IMG="$DIR/backup_mmcblk0p1_$(date +%Y%m%d_%H%M).img"
OUTFILE_FSA="$DIR/backup_mmcblk0p2_$(date +%Y%m%d_%H%M).fsa"

# Shut down some services before starting backup process
echo "Stopping some services before backup."
${BASEDIR}/backup-action.sh pre

# First sync disks
sync

# Begin the backup process, should take about 5 minutes using NFS and Gigabit ethernet
# raspberry linux usually have one fat partition and one ext4 partition
echo "Backing up SD card to NAS ${DIR}/"
echo "This will take some time. Please wait..."

# Backing up partition table
sfdisk -d /dev/mmcblk0 >"${DIR}/backup_partition_table"

# Backing up vfat partition
SIZE=$(parted --script /dev/mmcblk0 Unit B print | awk '/fat/{gsub(/B/, ""); print $4}')
umount -l /boot
dd if=/dev/mmcblk0p1 bs=1M conv=sync,noerror iflag=fullblock status=none | pv -tpreb -s $SIZE >$OUTFILE_IMG
RESULT1=$?
mount /boot

# Backing up ext4 partition
fsarchiver savefs -o $OUTFILE_FSA /dev/mmcblk0p2 -j4 -A
RESULT2=$?

# Start services that were shutdown before backup
echo "Starting services."
${BASEDIR}/backup-action.sh post

# If command has completed successfully, delete previous backups and exit
if [ $RESULT1 = 0 -a $RESULT2 = 0 ]; then
   echo -e "\e[1;49;92mRaspberryPI backup successfully completed\e[0m"
   echo "Old backup files will be deleted."
   ls ${DIR}/backup_mmcblk0* | grep -Ev "${OUTFILE_IMG}|${OUTFILE_FSA}" | xargs rm -f
   echo "FILES: $(basename $OUTFILE_IMG) $(basename $OUTFILE_FSA)"
   EXIT_STATUS=0
# Else remove attempted backup files
else
   echo -e "\e[1;49;91mRaspberryPI Backup failed.\e[0m"
   echo "Old backup files will be kept."
   echo "Please check if there is sufficient space on the NAS."
   rm -f $OUTFILE_IMG $OUTFILE_FSA
   EXIT_STATUS=1
fi

sleep 1
umount $MOUNTPOINT

echo -e "\e[1;49;92m==> script completed in:\e[0m $(convertsecs ${SECONDS})"
exit $EXIT_STATUS
