function _update_ps1() {
    PS1="$(/usr/local/bin/powerline-go -modules time,nix-shell,venv,user,host,ssh,cwd,perms,git,hg,jobs,exit,root -mode flat -error $?)"
    # PS1="$(/usr/local/bin/powerline-go -error $?)"
}
