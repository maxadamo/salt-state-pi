#!/bin/bash
#
git_pull() {
    git reset --hard
    git lfs pull
    git pull
}

for DIR in salt pillar; do
    cd /srv/$DIR
    git_pull &
done
