/usr/local/bin/powerline-go:
  file.managed:
    - source:
      - salt://files/powerline-go
    - user: root
    - group: root
    - mode: 755
/usr/local/bin/Raspberry_Pi_FSArchiver_Backup.sh:
  file.managed:
    - source:
      - salt://files/Raspberry_Pi_FSArchiver_Backup.sh
    - user: root
    - group: root
    - mode: 755
/etc/profile.d/powerline-go.sh:
  file.managed:
    - source:
      - salt://files/powerline-go.sh
    - user: root
    - group: root
    - mode: 644
/root/.bashrc:
  file.managed:
    - source:
      - salt://files/bashrc
    - user: root
    - group: root
    - mode: 644
/root/.bash_profile:
  file.managed:
    - source:
      - salt://files/bash_profile
    - user: root
    - group: root
    - mode: 644
/root/.bash_aliases:
  file.managed:
    - source:
      - salt://files/bash_aliases
    - user: root
    - group: root
    - mode: 644
/etc/salt/minion_id:
  file.managed:
    - source:
      - salt://files/minion_id.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 640
/etc/salt/minion:
  file.managed:
    - source:
      - salt://files/minion
    - user: root
    - group: root
    - mode: 644
