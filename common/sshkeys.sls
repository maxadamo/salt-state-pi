add_root_ssh_keys:
  ssh_auth.present:
    - user: 'root'
    - source: salt://files/id_rsa.pub
    - config: /%h/.ssh/authorized_keys
    - require:
      - user: 'root'
add_alarm_ssh_keys:
  ssh_auth.present:
    - user: 'alarm'
    - source: salt://files/id_rsa.pub
    - config: /%h/.ssh/authorized_keys
    - require:
      - user: 'alarm'
