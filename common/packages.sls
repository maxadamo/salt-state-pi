install_common_packages:
  pkg.installed:
    - pkgs:
      - bash-completion
      - bashtop
      - fsarchiver
      - parted
      - pv
      - rsync
      - screen
      - wol
