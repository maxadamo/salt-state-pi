/etc/dhcpd.conf:
  file.managed:
    - source:
      - salt://files/{{ grains['host'] }}/dhcpd.conf.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 644
