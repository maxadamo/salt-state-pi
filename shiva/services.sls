named:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - pkg: bind
dhcpd4:
  service.running:
    - enable: True
    - watch:
      - pkg: dhcp
      - file: /etc/dhcpd.conf
