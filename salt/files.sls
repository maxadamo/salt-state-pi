/etc/salt/master:
  file.managed:
    - source:
      - salt://files/{{ grains['host'] }}/master
    - user: root
    - group: root
    - mode: 640
/etc/salt/autosign.conf:
  file.managed:
    - source:
      - salt://files/{{ grains['host'] }}/autosign.conf
    - user: root
    - group: root
    - mode: 644
/usr/local/bin/salt-pull.sh:
  file.managed:
    - source:
      - salt://files/salt-pull.sh
    - user: root
    - group: root
    - mode: 755
