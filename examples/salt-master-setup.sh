#!/bin/bas

pacman -Syu --noconfirm
echo | pacman -S --needed --noconfirm base-devel 
pacman -Sy --needed --noconfirm vim community/python-pygit2 extra/python-pip community/python-pyaml extra/python-markupsafe community/python-pyzmq community/python-jinja community/python-pycryptodomex community/python-wheel git
pip install salt

mkdir /etc/salt

cat <<'EOF' >>/etc/salt/autosign.conf
*.flat.global
EOF

cat <<'EOF' >>/etc/salt/master
autosign_file: /etc/salt/autosign.conf
EOF

cat <<'EOF' >>/etc/systemd/system/salt-master.service
[Unit]
Description=The Salt Master Server
Documentation=man:salt-master(1) file:///usr/share/doc/salt/html/contents.html https://docs.saltstack.com/en/latest/contents.html
After=network.target

[Service]
LimitNOFILE=100000
Type=notify
NotifyAccess=all
ExecStart=/usr/bin/salt-master

[Install]
WantedBy=multi-user.target
EOF

cat <<'EOF' >>/etc/systemd/system/salt-minion.service
[Unit]
Description=The Salt Minion
Documentation=man:salt-minion(1) file:///usr/share/doc/salt/html/contents.html https://docs.saltstack.com/en/latest/contents.html
After=network.target salt-master.service

[Service]
KillMode=process
Type=notify
NotifyAccess=all
LimitNOFILE=8192
ExecStart=/usr/bin/salt-minion

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable salt-master.service
systemctl enable salt-minion.service